;
; normalization orchestra
;
        sr     = 44100
        ksmps  = 10
        nchnls = 2
        0dbfs  = 1.0



        instr 1
ifleft  = 1
ifright = 2
idur    = p3
iodur   = p4
ifreq   = 1/idur
iofreq  = 1/iodur
ifun    = 1
iflen   = ftlen(ifun)
itune   = p5
igain   = ampdb(p6)

kfreader phasor ifreq
ktimpt   table  kfreader, ifun, 1

aleft    pvoc   ktimpt, itune, ifleft
aright   pvoc   ktimpt, itune, ifright

ktime    times
         fprintks "pvoc_ktimpt.data", "%10.6f %20.18f %05d\n", ktime, ktimpt, int(kfreader*iflen)

         outs   aleft*igain, aright*igain
         endin
