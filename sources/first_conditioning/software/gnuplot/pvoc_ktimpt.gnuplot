#
# ktimpt data: time pointer of the pvoc vocoder in csound
#
set term pdfcairo
plot [0:80][0:80] 'pvoc_ktimpt.data' w lines lw 2 title 'pvoc time pointer'

#
# This does not work. We redirect to stdout
# set output "pvoc_ktimpt.pdf"
