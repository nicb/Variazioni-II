% -*- texinfo -*-
% @deftypefn {normalized_table.m} {@var{out} =} normalized_table(@var{tab}, @var{nidxs})
% @cindex 2 normalized_table
% @code{normalized_table} returns the value contained in a table according
% to the index @var{nidxs} (normalized between 0 and 1).
% It is supposed to simulate the csound @code{table} opcode in mode 1.
% @seealso{phasor}
% @end deftypefn
% 
function values = normalized_table(tab, nidxs)
  cols = size(tab, 2);
  idxs = floor(nidxs * (cols-1))+1;
  values = tab(idxs);
end
