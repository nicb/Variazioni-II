%
% This piece of code calculates the table that is required by csound
% to do the proper stretching of every anthem fragment according to the
% data that is contained in each directory in the 'metadata.m' file
%
% So this is the way to run this code:
%
% cat <dir>/metadata.m lead_stretches.m proj_calculator.m | octave -q
%
stretched_lead_time = lead_times(stretched_lead_time_index)
stretched_lead_time_minutes = floor(stretched_lead_time/60) 
stretched_lead_time_seconds = ((stretched_lead_time/60.0) - stretched_lead_time_minutes) * 60

%
% exponential function calculator to figure out the initial stretch
% metronome function
%
% 
% metro(t) = metro_0 + k^t - 1
%
% where metro_0 is the starting metronome time (which is supposed to be
% the end metronome time multiplied by constant inv_phi)
%
% thus, k is:
%
%     t1/------------------------  
% k = \/ metro_t1 - metro_t0 + 1
%     
% where metro_t0 is the initial metronome, metro_t1 is the final metronome
% t1 is the stretch_lead_time
%
inv_phi = 1 - (1/phis(1))
metro_1 = new_metronome;
metro_0 = new_metronome * inv_phi

k = (metro_1 - metro_0 + 1)**(1/stretched_lead_time);

funlen = 16384;
tstep = stretched_lead_time / funlen;
t = [0:tstep:stretched_lead_time-tstep];
metro = metro_0 + k.**(t) - 1;

plot(t, metro)
print('output.pdf', '-dpdf')

% for w=1:size(t,2) 
%   printf("%010.5f %010.5f\n", t(w), metro(w));
% end

