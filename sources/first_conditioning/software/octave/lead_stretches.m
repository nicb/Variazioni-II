%
% This calculates all the lead stretches according to the inital
% Russian anthem
%
orig_russian_lead_time = 19.237;
phi=1.6180339;
phi_positive_powers=[1:10];

phis = phi.**phi_positive_powers;
lead_times = phis * orig_russian_lead_time;
