% -*- texinfo -*-
% @deftypefn {phasor.m} {@var{out} =} phasor(@var{frq}, @var{dur}, @var{sr})
% @cindex 1 phasor
% @code{phasor} returns an increasing index from 0 to 1 distributed
% along a time axis of @code{@var{dur}} (seconds) @code{x @var{sr}} (sample rate).
% @var{frq} may be an array of frequences, but in this case it should match
% the dimensions of @{dur} x @{sr}.
% It is supposed to simulate the csound @code{phasor} opcode.
% @seealso{table}
% @end deftypefn
% 
function kout = phasor(frq, dur, sr)
  nsamples = floor(dur*sr);
  incs = frq ./ sr;
  if size(incs, 2) == 1
    incs = ones(1, nsamples) * incs;
  endif
  indxs = [1:nsamples];
  kout = zeros(1, nsamples);
  ph = 0;
  for k=2:nsamples
    ph = ph + incs(k);
    if (ph > 1.0)
      ph -= 1.0;
    end
    kout(k) = ph;
  endfor
  kout;
end
