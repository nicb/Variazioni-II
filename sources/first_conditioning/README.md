# Wave file conditioning

This file documents the way the concrete anthems should be prepared in order
to constitute the base material for the electro-acoustic base of the piece.

Basically, there are three main processes in action:

1. *Russian Normalization*: the russian version of the anthem is picked up as
   model
1. *Summing*: all versions - once normalized - are superimposed to constitute
   a single piece
1. *Mnemonic stretching*: a stretching process is then performed on the full
   superimposition

## Russian Normalization

1. When repeated several times, the pieces must be cut so that the last
   repetition is picked up
1. The last repetition is cut out and labeled in all its formal components
1. Single components are then adjusted to their russian counterparts

## Summing

## Mnemonic stretching

## Basic structure of the piece

The piece has 2 eight notes upbeat but is constantly shifted so the structure
is:

* prima strofa => 4 bars
* seconda strofa => 4 bars
* inciso 1 => 4 bars
* inciso 2 => 4 bars
* recap prima strofa (doppio periodo) => 8 bars
* recap seconda strofa (doppio periodo) => 6 bars
* finale rallentando => 2 bars

for a grand total of 128 onsets. Onsets picked up by essentia do differ to
some extent, but they are related to that figure.

All pieces need to be labeled in the same way. However, some versions do not
have the double period recapitulation (6 stanzas) but rather only 4.
