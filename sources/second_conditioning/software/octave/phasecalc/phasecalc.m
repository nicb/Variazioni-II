%
% phase calculation for amplitude modulation of all elements
%
T = [0:TIME_SAMPLING:TOTAL_TIME-TIME_SAMPLING];
amp = (1-OFFSET)/2.0;
als = (MAX_AMP-MIN_AMP)/WHERE;
bls = MIN_AMP;
ars = (MIN_AMP-MAX_AMP)/(TOTAL_TIME-WHERE);
brs = MAX_AMP;
tls = T(1:round((WHERE-TIME_SAMPLING)/TIME_SAMPLING));
trs = T(round(WHERE/TIME_SAMPLING):end);
ampls = als*tls + bls;
amprs = ars*(trs-WHERE) + brs;
amp = [ampls amprs];
freq_step=(MAX_MOD_FREQUENCY-BASE_MOD_FREQUENCY)/NUM_SONGS;
waves = zeros(NUM_SONGS, length(T));
phases = zeros(NUM_SONGS, 1);
for k=0:(NUM_SONGS-1)
  freq = BASE_MOD_FREQUENCY + (k*freq_step);
  w = freq * 2 * pi;
  outcos = cos(w * (T - WHERE));
  waves(k+1, :) = amp.* outcos + OFFSET .+ amp;
  phases(k+1) = acos(outcos(1))/(2*pi);
endfor

figure(1, "visible", "off")
plot(T, waves);
set(gca, "xtick", WHERE);
set(gca, "xgrid", "on");
axis([-0.01 60])
print("amplitude_of_singles.jpg", "-djpeg")

%
% print the csound gen table for the csound synthesizer
%
phf = fopen("phases.inc", "w");
fprintf(phf, "f100 0 16 -2 ");
for k=1:length(phases)
  fprintf(phf, "%+11.8f ", phases(k));
endfor
fprintf(phf, "; table of phase values for the AM modulation\n");
fflush(phf);
fclose(phf);

figure(2, "visible", "off")
subplot(2,1,1)
plot(T, amp)
subplot(2,1,2)
plot(T, sum(waves))
print("functions.jpg", "-djpeg")

%
% print the constants needed by csound
%
chf = fopen("constants.inc", "w");
fprintf(chf, "giwhere    =          %f\n", WHERE);
fprintf(chf, "gimaxamp   =          %f\n", MAX_AMP);
fprintf(chf, "giminamp   =          %f\n", MIN_AMP);
fprintf(chf, "gibasefrq  =          %f\n", BASE_MOD_FREQUENCY);
fprintf(chf, "gifreqstep =          %f\n", freq_step);
fflush(chf);
fclose(chf);
