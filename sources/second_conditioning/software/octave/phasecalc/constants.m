%
% This is the point in time where all amplitude modulation
% cosines have to be combined
%
WHERE=19.930;
%
% number of songs
%
NUM_SONGS=14;
%
% total reference time
%
TOTAL_TIME=75.9146;
TIME_SAMPLING=0.001;
%
% Base frequency
%
BASE_NCYCLES      = 0.07;
BASE_MOD_FREQUENCY=BASE_NCYCLES/WHERE;
%
% Max frequency
%
MAX_NCYCLES       = 1.07;
MAX_MOD_FREQUENCY=MAX_NCYCLES/WHERE;
%
% Maximum amplitude (when cosines are sparse)
%
MIN_AMP = 1/5;
%
% Minimum amplitude (when cosines are compact)
%
MAX_AMP = 1/2;
%
% Minumum amplitude modulation offset
%
OFFSET=MIN_AMP/2;
