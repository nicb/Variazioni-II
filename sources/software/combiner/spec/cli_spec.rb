RSpec.describe 'Combiner CLI' do

  before :example do
    @default_path = '.'
    @base_path = File.join(Combiner::Test::FIXTURE_PATH, 'mocks')
    @dest_path = File.join(Combiner::Test::TMP_PATH, 'mocks')
    @exe_path = File.expand_path(File.join(['..'] * 2, 'exe', 'combine'), __FILE__)
    @command_line = "#{@exe_path} #{@base_path} #{@dest_path}"
  end

  after :example do
    FileUtils.rm_f(Dir.glob(File.join(@dest_path, '*')))
  end

  it 'does work' do
    IO::popen(@command_line) do
      |io|
      lines = io.readlines.join
      expect(lines).to match(%r{^.*<CsoundSynthesizer>.*$})
      expect(lines).to match(%r{^.*<CsScore>.*$})
      expect(lines).to match(%r{^i1.*$})
      expect(lines).to match(%r{^.*<\/CsScore>.*$})
      expect(lines).to match(%r{^.*<\/CsoundSynthesizer>.*$})
    end
    expect((files = Dir.glob(File.join(@dest_path, 'soundin.*'))).size).to eq(VII::Common::LANGUAGES.size)
    files.each { |f| expect(File.exist?(f)).to(be(true), f) }
  end

end
