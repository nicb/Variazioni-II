RSpec.describe Combiner::Generator do

  before :example do
    @default_path = '.'
    @base_path = File.join(Combiner::Test::FIXTURE_PATH, 'mocks')
    @dest_path = File.join(Combiner::Test::TMP_PATH, 'mocks')
  end

  it 'can be created' do
    expect((g = Combiner::Generator.new)).not_to be nil
    expect(g.base_path).to eq(@default_path)
    expect(g.dest_path).to eq(@default_path)
  end

  it 'can be created with specific paths' do
    expect((g = Combiner::Generator.new(@base_path, @dest_path))).not_to be nil
    expect(g.base_path).to eq(@base_path)
    expect(g.dest_path).to eq(@dest_path)
    expect(Combiner::VII_SET.base_path).to eq(@base_path)
    expect(Combiner::VII_SET.dest_path).to eq(@dest_path)
  end

  it 'creates a proper csound file' do
    expect((g = Combiner::Generator.new(@base_path, @dest_path))).not_to be nil
    expect((csfile = g.to_csound)).not_to be nil
    expect(csfile).to match(/^.*<CsoundSynthesizer>$/)
    expect(csfile).to match(/^.*<CsScore>$/)
    expect(csfile).to match(/^.*<\/CsScore>$/)
    expect(csfile).to match(/^.*<\/CsoundSynthesizer>$/)
  end

end
