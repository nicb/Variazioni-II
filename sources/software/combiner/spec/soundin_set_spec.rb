RSpec.describe Combiner::SoundinSet do

  before :example do
    @base_path = File.join(Combiner::Test::FIXTURE_PATH, 'mocks')
    @dest_path = File.join(Combiner::Test::TMP_PATH, 'mocks')
    @num_of_files = VII::Common::LANGUAGES.size
  end

  after :example do
    FileUtils.rm_f(Dir.glob(File.join(@dest_path, '*')))
  end

  it 'cannot be created' do
    expect { Combiner::SoundinSet.new(@base_path, @dest_path) }.to raise_error(NoMethodError)
  end

  it 'does exist, instead, on a singleton basis' do
    expect(Combiner::VII_SET).not_to be nil
  end

  it 'has all the languages included in Variazioni II' do
    expect(Combiner::VII_SET.size).to eq(VII::Common::LANGUAGES.size)
  end

  it 'actually does create the required soundins' do
    expect(ss = Combiner::SoundinSet.create(@base_path, @dest_path)).not_to be nil
    ss.link
    m = Dir.glob(File.join(@dest_path, 'soundin.*'))
    expect(m.size).to eq(@num_of_files)
  end

  it 'may change the base and destination paths afterwards' do
    expect(ss = Combiner::SoundinSet.create).not_to be nil
    expect(ss.base_path = @base_path).to eq(@base_path)
    expect(ss.dest_path = @dest_path).to eq(@dest_path)
    ss.link
    m = Dir.glob(File.join(@dest_path, 'soundin.*'))
    expect(m.size).to eq(@num_of_files)
  end

end
