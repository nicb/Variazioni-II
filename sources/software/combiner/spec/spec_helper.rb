require 'bundler/setup'
require 'byebug'
require 'combiner'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

module Combiner
  module Test
    FIXTURE_PATH = File.expand_path(File.join('..', 'fixtures'), __FILE__)
    TMP_PATH     = File.expand_path(File.join(['..'] * 2, 'tmp'), __FILE__)
  end
end
