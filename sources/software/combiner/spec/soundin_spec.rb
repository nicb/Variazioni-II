require 'wavefile'

RSpec.describe Combiner::Soundin do

  before :example do
    @number = 7
    @base_path = File.join(Combiner::Test::FIXTURE_PATH, 'mocks')
    @dest_path = File.join(Combiner::Test::TMP_PATH, 'mocks')
    @language = VII::Common::LANGUAGES.first
    @dest = File.join(@dest_path, 'soundin.' + ("%d" % @number))
    @dur = WaveFile::Duration.new(320210145, 44100) # 2 hours, 1 minute, 1 second, 1 msec
    dur_hours = 2.0
    dur_minutes = dur_seconds = dur_milliseconds = 1.0
    @total_duration_string = (dur_hours * 3600 + dur_minutes * 60 + dur_seconds + dur_milliseconds * 0.001).to_s
    @real_wav = File.join(Combiner::Test::FIXTURE_PATH, 'real_wave_file.wav') # lasts 1 second, 1 msec
    @rw_total_duration_string = '1.002'
  end

  after :example do
    FileUtils.rm_f(@dest)
  end

  it 'can be created' do
    expect(Combiner::Soundin.new(@language, @number, @base_path, @dest_path)).not_to be nil
  end

  it 'does indeed work' do
    expect((s = Combiner::Soundin.new(@language, @number, @base_path, @dest_path))).not_to be nil
    s.link
    expect(File.exist?(@dest)).to be true
    expect(File.symlink?(@dest)).to be true
  end

  it 'calculates a duration properly' do
    expect((s = Combiner::Soundin.new(@language, @number))).not_to be nil
    expect(s.send(:duration_in_seconds_to_s, @dur)).to eq(@total_duration_string)
  end

  it 'calculates a duration properly even with a real wave file' do
    expect((s = Combiner::Soundin.new(@language, @number, @base_path))).not_to be nil
    expect(s.dur).to eq(@rw_total_duration_string)
  end

end
