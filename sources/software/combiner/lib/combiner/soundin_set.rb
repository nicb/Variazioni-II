require 'vII/common'

module Combiner

  class SoundinSet < Array
    attr_reader :base_path, :dest_path
    private_class_method :new

    def initialize(b = '.', d = '.')
      @base_path = b
      @dest_path = d
    end

    def link
      self.each { |s| s.link }
    end

    def base_path=(b)
      @base_path = b
      rebuild
    end

    def dest_path=(d)
      @dest_path = d
      rebuild
    end

    class << self

      def create(b = '.', d = '.')
        res = new(b, d)
        res.send(:create_set)
        res
      end

    end

  private

    def rebuild
      self.clear
      create_set
    end

    def create_set
      n = 0
      VII::Common::LANGUAGES.each do
        |l|
        n += 1
        self << Soundin.new(l, n, self.base_path, self.dest_path)
      end
      n
    end

  end

  VII_SET = SoundinSet.create

end

