require 'fileutils'
require 'wavefile'

module Combiner

  class Soundin
    attr_reader :language, :source, :base, :destination, :number

    PREFIX = 'soundin.'

    def initialize(l, n, b = '.', d = '.')
      @language = l
      @destination = d
      @base = File.expand_path(b)
      @source = File.join(self.base, self.language, self.language.downcase + VII::Common::NORM_WAV_SUFFIX)
      @number = n
    end

    def link
      FileUtils.ln_sf(self.source, d_path)
    end

    def dur
      calculate_duration
    end

  private

    def d_path
      File.join(self.destination, PREFIX + numstring)
    end

    def numstring
      "%d" % [ self.number ]
    end

    def calculate_duration
      w = WaveFile::Reader.new(self.source)
      d = w.total_duration
      duration_in_seconds_to_s(d)
    end

    #
    # the precise WaveFile::Duration calculation
    # does not do the proper rounding for 
    # milliseconds so we do it a bit better
    #
    def duration_in_seconds_to_s(d)
      res = (d.hours * 3600).to_f
      res += (d.minutes * 60)
      res += d.seconds
      msec = (((d.sample_frame_count/d.sample_rate.to_f)-res)*1000.0).round
      res += (msec * 0.001)
      "%.3f" % res
    end

  end

end
