#
# Csound generator for the combination of soundins
#

require 'erb'

module Combiner

  class Generator
    attr_reader :base_path, :dest_path

    CSOUND_CSD_TEMPLATE = File.expand_path(File.join(['..'] * 3, 'templates', 'csound', 'combiner.csd.erb'), __FILE__)

    def initialize(b = '.', d = '.')
      @base_path = b
      @dest_path = d
      setup_paths
    end

    def to_csound
      template = File.open(CSOUND_CSD_TEMPLATE, 'r') { |fh| fh.readlines.join }
      render = ERB.new(template)
      render.result(binding)
    end

  protected

    def to_csound_score
      res = csound_score_header
      VII_SET.each do
        |s|
        res += ("i1 %12.9f %-12s %2d; %-2s\n" % [ 0.0, s.dur, s.number, s.language ])
      end
      res += csound_score_trailer
      res
    end

  private

    def csound_score_header
      ''
    end

    def csound_score_trailer
      "e\n"
    end

    def setup_paths
      VII_SET.base_path = self.base_path
      VII_SET.dest_path = self.dest_path
    end

  end

end
