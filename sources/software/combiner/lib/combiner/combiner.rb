#
# General combiner
#
module Combiner

  class Combiner
    attr_reader :base_path, :dest_path

    def initialize(b = '.', d = '.')
      @base_path = b
      @dest_path = d
    end

    def combine
      VII_SET.base_path = self.base_path
      VII_SET.dest_path = self.dest_path
      VII_SET.link
      g = Generator.new(self.base_path, self.dest_path)
      g.to_csound
    end

  end

end
