require 'bundler/setup'

module Combiner
  PATH = File.expand_path(File.join('..', 'combiner'), __FILE__)
end

%w{
  version
  soundin
  soundin_set
  generator
  combiner
}.each { |f| require File.join(Combiner::PATH, f) }
