RSpec.describe Comparator::Result do

  before :example do
    @message = 'test message'
  end

  it 'cannot be created as abstract class' do
    expect { Comparator::Result.new(0) }.to raise_error NoMethodError
  end

  it 'can be created as a Match class' do
    expect((r = Comparator::Match.new('!', 0))).not_to be nil
    expect(r.kind_of?(Comparator::Result)).to be true
    expect(r.is_a?(Comparator::Match)).to be true
    expect(r.match?).to be true
  end

  it 'can be created as a Match class and have a message' do
    expect((r = Comparator::Match.new('!', 0, @message))).not_to be nil
    expect(r.message).to eq(@message)
  end

  it 'can be created as a NoMatch class' do
    expect((r = Comparator::NoMatch.new('X', -1))).not_to be nil
    expect(r.kind_of?(Comparator::Result)).to be true
    expect(r.is_a?(Comparator::NoMatch)).to be true
    expect(r.match?).to be false
  end

  it 'can be created as a NoMatch class and have a message' do
    expect((r = Comparator::NoMatch.new('XX', 1, @message))).not_to be nil
    expect(r.message).to eq(@message)
  end

end
