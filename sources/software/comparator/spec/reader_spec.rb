RSpec.describe Comparator::Reader do

  before :example do
    @language = 'TEST'
    @manual_keys = %w{metadata analysis}
    @essentia_keys = %w{lowlevel metadata rhythm tonal}
    @basepath = File.expand_path(File.join('..', 'fixtures'), __FILE__)
  end

  it 'can be created' do
    expect(Comparator::Reader.new(@language, @basepath)).not_to be nil
  end

  it 'reads the manual features' do
    expect((c = Comparator::Reader.new(@language, @basepath))).not_to be nil
    expect(c.manual.is_a?(Hash)).to be true
    @manual_keys.each { |k| expect(c.manual.keys.include?(k)).to be true }
  end


  it 'reads the essentia features' do
    expect((c = Comparator::Reader.new(@language, @basepath))).not_to be nil
    expect(c.essentia.is_a?(Hash)).to be true
    @essentia_keys.each { |k| expect(c.essentia.keys.include?(k)).to(be(true), k) }
  end


end
