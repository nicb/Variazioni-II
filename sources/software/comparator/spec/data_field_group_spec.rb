RSpec.describe Comparator::DataFieldGroup do

  before :example do
    @language = 'TESTDFG'
    @manual_test_data = 'a'
    @man_keys = ['key_one', 'key_two', 'key_three', 'key_four' ]
    @auto_test_data = 'b'
    @compare_result = @manual_test_data <=> @auto_test_data
    @auto_keys = ['key_one', 'key_two', 'key_three', ]
    @basepath = Comparator::Test::TEST_FIXTURES_BASE_PATH
    @num_test_key = 'test_number'
    @man_num_test_data = 500
    @auto_num_test_data = 400
    @num_compare_result = @man_num_test_data <=> @auto_num_test_data
    @key = "%s::man.%s_vs_auto.%s" % [ @language, @man_keys.last, @auto_keys.last ]
    @wrong_key = ['wrong']
    @log = StringIO.new
    Comparator::LOG.reopen(@log)
  end

  it 'can be created with an array of keys' do
    expect(Comparator::DataFieldGroup.new(@language, @man_keys, @auto_keys, @basepath)).not_to be nil
  end

  it 'does compare the actual data (ascii)' do
    expect((c = Comparator::DataFieldGroup.new(@language, @man_keys, @auto_keys, @basepath))).not_to be nil
    expect(c.compare.result).to eq(@compare_result)
  end

  it 'does compare the actual data (numeric)' do
    expect((c = Comparator::DataFieldGroup.new(@language, @num_test_key, @num_test_key, @basepath))).not_to be nil
    expect(c.compare.result).to eq(@num_compare_result)
  end

  it 'has a working key method' do
    expect((c = Comparator::DataFieldGroup.new(@language, @man_keys, @auto_keys, @basepath))).not_to be nil
    expect(c.key).to eq(@key)
  end

  it 'is rigid with non existing auto data keys' do
    expect((c = Comparator::DataFieldGroup.new(@language, @man_keys, @wrong_key, @basepath))).not_to be nil
    expect { c.compare }.to raise_error(Comparator::DataField::NotFound)
  end

  it 'is less rigid with non existing manual data keys' do
    expect((c = Comparator::DataFieldGroup.new(@language, @wrong_key, @auto_keys, @basepath))).not_to be nil
    expect(c.compare).to be nil
  end

end
