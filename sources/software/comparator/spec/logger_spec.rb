RSpec.describe Comparator::Logger do

  it 'is a singleton that cannot be created anew' do
    expect { Comparator::Logger.new }.to raise_error NoMethodError
  end
  
  it 'does already exist, however' do
    expect(Comparator::LOG).not_to be nil
  end

  it 'is of the proper class type' do
    expect(Comparator::LOG.class).to be Comparator::Logger
  end

end
