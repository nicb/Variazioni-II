require 'yaml'

RSpec.describe Comparator::DataFieldList do

  before :example do
    @language = 'TESTDFG'
    @basepath = Comparator::Test::TEST_FIXTURES_BASE_PATH
    @config_file = File.join(@basepath, 'config.yml')
    @config_data = YAML.load(File.open(@config_file, 'r'))
    @result_size = @config_data.keys.size
    @wresults = @rresults = 0
    @config_data.each do
       |k, v|
       if v['should_raise'] == 'no'
         @rresults += 1
       else
         @wresults += 1
       end
    end
  end

  it 'can be created' do
    expect(Comparator::DataFieldList.new(@language, @config_file, @basepath)).not_to be nil
  end

  it 'does compare the actual data returning the appropriate results' do
    expect((c = Comparator::DataFieldList.new(@language, @config_file, @basepath))).not_to be nil
    expect(c.compare).to be nil
    wres = rres = 0
    c.results.each { |k, cr| wres += 1 if cr.class == Comparator::NoMatch }
    c.results.each { |k, cr| rres += 1 if cr.class == Comparator::Match }
    expect(wres).to eq(@wresults)
    expect(rres).to eq(@rresults)
  end

end
