RSpec.describe Comparator::DataField do

  before :example do
    @test_data = 'test data'
    @keys = ['one', 'two', 'three', 'four' ]
    @wrong_keys = ['one', 'two', 'seven']
    @sym_keys = [ :one, :two, :three, :four ]
    @data = { @keys[0] => { @keys[1] => { @keys[2] => { @keys[3] => @test_data }}}}
    @log = StringIO.new
    Comparator::LOG.reopen(@log)
    @log_msg = /INFO -- : Key "seven" not found/
  end

  it 'can be created with an array of keys' do
    expect(Comparator::DataField.new(@data, @keys)).not_to be nil
  end

  it 'can be created with a single key' do
    expect((c = Comparator::DataField.new(@data, @keys[0]))).not_to be nil
    expect(c.keys.kind_of?(Array)).to be true
  end

  it 'can be created with an array of symbolic keys' do
    expect(Comparator::DataField.new(@data, @sym_keys)).not_to be nil
  end

  it 'does find the actual datum' do
    expect((c = Comparator::DataField.new(@data, @keys))).not_to be nil
    expect(c.datum).to eq(@test_data)
  end

  it 'handles wrong or non-existing keys gracefully logging them' do
    expect((c = Comparator::DataField.new(@data, @wrong_keys))).not_to be nil
    expect(c.datum).to be nil
    expect(@log.string).to match(@log_msg)
  end

end
