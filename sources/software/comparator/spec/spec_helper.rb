require "bundler/setup"
require "comparator"
require 'byebug'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

module Comparator::Test

  TEST_FIXTURES_BASE_PATH = File.expand_path(File.join('..', 'fixtures'), __FILE__)

end
