RSpec.describe Comparator do

  before :example do
    @class_methods = [:log, :to_yaml]
    @language = 'TESTDFG'
    @basepath = Comparator::Test::TEST_FIXTURES_BASE_PATH
    @config_file = File.join(@basepath, 'config.yml')
    @test_io = StringIO.new
    Comparator::LOG.reopen(@test_io)
    @yaml_output = "--- !ruby/hash:Comparator::ResultSet\nTESTDFG::man.key_four_vs_auto.key_three: !ruby/object:Comparator::NoMatch\n  status: false\n  key: TESTDFG::man.key_four_vs_auto.key_three\n  result: -1\n  message: a != b\nTESTDFG::man.test_number_vs_auto.test_number: !ruby/object:Comparator::NoMatch\n  status: false\n  key: TESTDFG::man.test_number_vs_auto.test_number\n  result: 1\n  message: 500 != 400\nTESTDFG::man.test_equal_vs_auto.test_equal: !ruby/object:Comparator::Match\n  status: true\n  key: TESTDFG::man.test_equal_vs_auto.test_equal\n  result: 0\n  message: ''\n"
    @log_output_regexps = [/^I, .*  INFO -- rspec: TESTDFG::man.key_four_vs_auto.key_three: a != b \(-1\)$/, /^I, .*  INFO -- rspec: TESTDFG::man.test_number_vs_auto.test_number: 500 != 400 \(1\)$/, /^I, .*  INFO -- rspec: TESTDFG::man.test_equal_vs_auto.test_equal: match$/ ]
  end

  it "has a version number" do
    expect(Comparator::VERSION).not_to be nil
  end

  it "responds to the [#{@class_methods}] class methods" do
    @class_methods.each { |m| expect(Comparator.respond_to?(m)).to be true }
  end

  it 'has a functional :log class method' do
    expect(Comparator.log(@language, @config_file, @basepath)).not_to be nil
    expect(@test_io.string.empty?).to be false
    log_output = @test_io.string.split("\n")
    log_output.each_index { |idx| expect(log_output[idx]).to match(@log_output_regexps[idx]) }
  end

  it 'has a functional :to_yaml class method' do
    expect((y = Comparator.to_yaml(@language, @config_file, @basepath))).not_to be nil
    expect(y.empty?).to be false
    expect(y).to eq(@yaml_output)
  end

end
