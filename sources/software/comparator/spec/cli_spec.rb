require 'tempfile'

RSpec.describe 'Comparator command line interface' do

  before :example do
    @language = 'TESTDFG'
    @basepath = Comparator::Test::TEST_FIXTURES_BASE_PATH
    @config_file = File.join(@basepath, 'config.yml')
    @config_data = YAML.load(File.open(@config_file, 'r'))
    @methods = [:log, :to_yaml]
    @log = Tempfile.new('comparator')
    @log.close
    Comparator::LOG.reopen(@log)
    @exe = File.expand_path(File.join(['..'] * 2, 'exe', 'comparator'), __FILE__)
    @should_log =
    [
      %r{^I, \[.*\]  INFO -- comparator: TESTDFG::man.key_four_vs_auto.key_three: a != b \(-1\)$},
      %r{^I, \[.*\]  INFO -- comparator: TESTDFG::man.test_number_vs_auto.test_number: 500 != 400 \(1\)$},
      %r{^I, \[.*\]  INFO -- comparator: TESTDFG::man.test_equal_vs_auto.test_equal: match$},
    ]
  end

  after :example do
    @log.close
    @log.unlink
  end

  it 'does respond to the log and to_yaml methods' do
    @methods.each { |m| expect(Comparator.respond_to?(m)).to be true }
  end

  it 'can be run as event logger' do
    expect(system("COMPARATOR_LOGGER_OUTPUT=\"#{@log.path}\" #{@exe} #{@language} #{@config_file} #{@basepath}")).to be true
    log_out = nil
    File.open(@log.path, 'r+') { |fh| log_out = fh.readlines }
    @should_log.each_index { |idx| expect(log_out[idx]).to(match(@should_log[idx]), log_out[idx]) }
  end

end
