require 'yaml'

RSpec.describe Comparator::ResultSet do

  before :example do
    @message = 'test message'
    @rresult = Comparator::Match.new('!', 0)
    @wresult = Comparator::NoMatch.new('X', 1, @message)
    @yaml_output = "--- !ruby/hash:Comparator::ResultSet\n\"!\": !ruby/object:Comparator::Match\n  status: true\n  key: \"!\"\n  result: 0\n  message: ''\nX: !ruby/object:Comparator::NoMatch\n  status: false\n  key: X\n  result: 1\n  message: test message\n"
  end

  it 'can be created' do
    expect(Comparator::ResultSet.new).not_to be nil
  end

  it 'has a working << operator method' do
    expect((rs = Comparator::ResultSet.new)).not_to be nil
    expect(rs << @rresult).not_to be nil
    expect(rs << @wresult).not_to be nil
  end

  it 'has an << operator method protectd against misuse' do
    expect((rs = Comparator::ResultSet.new)).not_to be nil
    expect { rs << 23 }.to raise_error Comparator::ResultSet::NotAResult
  end

  it 'inherits a workable .to_yaml method from Hash' do
    expect((rs = Comparator::ResultSet.new)).not_to be nil
    expect(rs << @rresult).not_to be nil
    expect(rs << @wresult).not_to be nil
    expect(rs.to_yaml).to eq(@yaml_output)
  end

end
