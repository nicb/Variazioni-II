module Comparator

  class Result   
    attr_reader :result, :key, :message, :status
    private_class_method :new # abstract, should not be created

    def initialize(k, res, msg = '')
      @key = k
      @result = res
      @message = msg
    end

    alias_method :match?, :status
  end

  class Match < Result
    public_class_method :new

    def initialize(k, res, msg = '')
      @status = true
      super
    end
  end

  class NoMatch < Result
    public_class_method :new

    def initialize(k, res, msg = 'No Message')
      @status = false
      super
    end
  end

end
