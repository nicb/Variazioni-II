module Comparator

  class DataField

    attr_reader :resource, :keys

    class NotFound < ArgumentError; end

    def initialize(r, k)
      k = [ k ] unless k.kind_of?(Array)
      @keys = k.map { |kk| kk.to_s }
      @resource = r
    end

    def datum
      @__res__ = nil
      begin
        recurse(self.resource, 0)
      rescue NotFound => msg
        LOG.info(msg)
      end  
      @__res__
    end

  private

    def recurse(rsrc, idx)
      raise(NotFound, "Key \"#{self.keys[idx]}\" not found") unless rsrc[self.keys[idx]]
      if (self.keys.size-1 > idx)
        recurse(rsrc[self.keys[idx]], idx+1)
      else
        @__res__ = rsrc[self.keys[idx]]
      end
      @__res__
    end

  end

end
