module Comparator

  class ResultSet < Hash

    class NotAResult < ArgumentError; end

    def <<(r)
      raise NotAResult, "value (#{r.inspect}) is not a Comparator::Result class" unless r.kind_of?(Result)
      self[r.key] = r
    end

    #
    # +to_yaml+
    #
    # a somewhat cleaner(?) version of +to_yaml+
    #
    def to_yaml
      super.gsub(/^---.*\\n/, '')
    end

    #
    # +log+
    #
    # writes into the system logger object
    #
    def log
      self.each do
        |k, v|
        msg = "%-20s: " % k
        msg += (v.match? ? "match" : ("%s (%s)" % [v.message, v.result]))
        LOG.info(File.basename($0)) { msg }
      end
    end

  end

end
