module Comparator

  class DataFieldGroup

    attr_reader :language, :man_field, :auto_field

    def initialize(lang, m_keys, a_keys, b = '.')
      @language = lang
      @base = b
      @reader = Reader.new(self.language, @base)
      @man_field = DataField.new(@reader.manual, m_keys)
      @auto_field = DataField.new(@reader.essentia, a_keys)
    end

    #
    # +compare+
    #
    # compare the two data and save the result.
    # Be strict with missing auto data keys and more relaxed with
    # missing manual keys.
    #
    def compare
      res = nil
      raise DataField::NotFound, "Auto data key set \"#{self.auto_field.keys.map {|k| k.to_s}.join('->')}\"" unless self.auto_field.datum
      return res unless self.man_field.datum
      comp = self.man_field.datum <=> self.auto_field.datum
      if comp == 0
        res = Match.new(self.key, comp)
      else
        res = NoMatch.new(self.key, comp, "%s != %s" % [ self.man_field.datum.to_s, self.auto_field.datum.to_s ])
      end
      res
    end

    #
    # +key()+
    #
    # key returns a string which is the combination of the two sides of the
    # datagroup, suitable to be used in ResultSet objects. Key will be a
    # string in the format "<language>::man.<last man key>_vs_auto.<last auto key>"
    #
    def key
      "%s::man.%s_vs_auto.%s" % [self.language, self.man_field.keys.last, self.auto_field.keys.last]
    end

  end

end
