require 'logger'

module Comparator

  class Logger < ::Logger

    private_class_method :new

    LOGGER_ENVIRONMENT = 'COMPARATOR_LOGGER_OUTPUT'
    LOGGER_OUTPUT     = ::ENV[LOGGER_ENVIRONMENT] || STDERR

    def initalize(out)
      super(out)
      self.level = ::Logger::INFO
    end

  end

  LOG = Logger.send(:new, Logger::LOGGER_OUTPUT)

end
