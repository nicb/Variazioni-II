require 'yaml'

module Comparator

  class Reader

    attr_reader :base, :language, :manual, :essentia

    def initialize(lang, b = '.')
      @language = lang
      @base = b
      read_manual()
      read_essentia()
    end

  private

    MANUAL_SFX = '-meta.y*ml'
    ESSENTIA_SFX = '-features.y*ml'

    def read_manual()
      f = self.language.downcase + MANUAL_SFX
      @manual = read_common(f)
    end

    def read_essentia()
      f = self.language.downcase + ESSENTIA_SFX
      @essentia = read_common(f)
    end

    def read_common(file)
      pathname = Dir.glob(File.join(self.base, file)).first
      YAML.load(File.open(pathname, 'r+'))
    end

  end

end
