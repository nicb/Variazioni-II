module Comparator

  VERSION = "0.0.0"
  PATH = File.expand_path(File.join(['..'] * 3, 'lib', 'comparator'), __FILE__)
  CONFIGURATION_FILE = File.expand_path(File.join(['..'] * 3, 'config', 'data_to_compare.yml'), __FILE__)

end
