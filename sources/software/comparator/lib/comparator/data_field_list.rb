require 'yaml'

module Comparator

  class DataFieldList

    attr_reader :language, :configuration, :base, :results

    class Differs < StandardError; end

    def initialize(lang, conf = COMPARATOR_CONFIGURATION_FILE, b = '.')
      @base = b
      @language = lang
      @configuration = conf
      @data_groups = setup_data_groups
      @results = ResultSet.new
    end

    def compare(&block)
      @data_groups.each do
         |dg|
         res = dg.compare(&block)
         self.results.<<(res) if res
      end
      nil
    end

  private

    def setup_data_groups
      res = []
      conf = YAML.load(File.open(self.configuration, 'r'))
      conf.keys.each { |k| res << DataFieldGroup.new(self.language, conf[k]['manual'], conf[k]['auto'], self.base) }
      res
    end

  end

end
