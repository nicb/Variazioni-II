module Comparator

  class << self

    def log(lang, conf = CONFIGURATION_FILE, b = '.')
      results(lang, conf, b).log
    end

    def to_yaml(lang, conf = CONFIGURATION_FILE, b = '.')
      results(lang, conf, b).to_yaml
    end

  private

    def results(lang, conf, b)
      df = DataFieldList.new(lang, conf, b)
      df.compare
      df.results
    end

  end

end
