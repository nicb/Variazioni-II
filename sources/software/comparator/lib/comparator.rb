require "comparator/constants"

%w{
  logger
  reader
  data_field
  result
  result_set
  data_field_group
  data_field_list
  comparator
}.each { |f| require File.join(Comparator::PATH, f) }
