module VII
  module Common

    #
    # for several ordering reasons RU must be the first of the array
    #
    LANGUAGES = %w{RU CAT DE DK FR IT JP NL NO PAL PT SP Toscanini UK}.freeze
    NORM_WAV_SUFFIX = '-normalized.wav'

  end
end
