module VII
  module Common

    PATH = File.expand_path(File.join('..', 'common'), __FILE__)

  end
end

%w{
  version
  constants
}.each { |f| require File.join(VII::Common::PATH, f) }
