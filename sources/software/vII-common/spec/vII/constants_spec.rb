RSpec.describe 'VII::Common constants' do

  it 'has a LANGUAGES constant' do
    expect(VII::Common::LANGUAGES).not_to be nil
    expect(VII::Common::LANGUAGES.class).to eq(Array)
  end

  it 'has a NORM_WAV_SUFFIX constant' do
    expect(VII::Common::NORM_WAV_SUFFIX).not_to be nil
    expect(VII::Common::NORM_WAV_SUFFIX.class).to eq(String)
  end

end
