RSpec.describe MmTracker::BeatsReader do

  before :example do
    @fixture_file = File.join(FIXTURE_PATH, 'features.yml')
  end

  it 'does read a feature file' do
    expect((mmt = MmTracker::BeatsReader.new(@fixture_file))).not_to be nil
    expect(mmt.beats_position).not_to be nil
    expect(mmt.beats_position.is_a?(Array)).to be true
  end

end
