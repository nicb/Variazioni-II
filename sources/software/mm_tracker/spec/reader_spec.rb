RSpec.describe MmTracker::Reader do

  before :example do
    @file = 'not_a_file'
  end

  it 'cannot be created' do
    expect { MmTracker::Reader.new(@file) }.to raise_error(NoMethodError)
  end

  it 'has an inner pure virtual function to read data' do
    expect { MmTracker::Reader.send(:new, @file) }.to raise_error(MmTracker::Reader::PureVirtualFunction)
  end

end
