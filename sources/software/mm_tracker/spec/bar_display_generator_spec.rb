RSpec.describe MmTracker::BarDisplayGenerator do

  before :example do
    @fixture_file = File.join(FIXTURE_PATH, 'bars.txt')
    csound_score_result_filename = File.join(FIXTURE_PATH, 'csound_score_output-bars.txt')
    csound_csd_result_filename = File.join(FIXTURE_PATH, 'csound_csd_output-bars.txt')
    @csound_lines = File.open(csound_score_result_filename, 'r') { |fh| fh.readlines.join }
    @csd_lines = File.open(csound_csd_result_filename, 'r') { |fh| fh.readlines.join }
  end

  it 'does read a feature file' do
    expect((dg = MmTracker::BarDisplayGenerator.new(@fixture_file))).not_to be nil
    expect(dg.reader.elements).not_to be nil
    expect(dg.reader.elements.is_a?(Array)).to be true
  end

  it 'does create the proper csound score string' do
    expect((dg = MmTracker::BarDisplayGenerator.new(@fixture_file))).not_to be nil
    expect(dg.send(:to_csound_score)).to eq(@csound_lines)
  end

  it 'does create the proper csound .csd file' do
    expect((dg = MmTracker::BarDisplayGenerator.new(@fixture_file))).not_to be nil
    expect(dg.to_csound).not_to be nil
    expect(dg.to_csound).to eq(@csd_lines)
  end

end

