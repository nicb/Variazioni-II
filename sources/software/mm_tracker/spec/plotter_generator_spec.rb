RSpec.describe MmTracker::PlotterGenerator do

  before :example do
    @fixture_file = File.join(FIXTURE_PATH, 'features.yml')
    @data_regexp = %r{^\s*[\d\.]+\s+[\d\.]+$}
  end

  it 'does read a feature file' do
    expect((pg = MmTracker::PlotterGenerator.new(@fixture_file))).not_to be nil
    expect(pg.reader.elements).not_to be nil
    expect(pg.reader.elements.is_a?(Array)).to be true
  end

  it 'does create the proper gnuplot string' do
    expect((pg = MmTracker::PlotterGenerator.new(@fixture_file))).not_to be nil
    expect(pg.send(:gnuplot_data)).to match(@data_regexp)
  end

  it 'does create the proper gnuplot string' do
    expect((pg = MmTracker::PlotterGenerator.new(@fixture_file))).not_to be nil
    expect((out = pg.to_gnuplot)).not_to be nil
    expect(out).to match(/^plot '-'/)
  end

end


