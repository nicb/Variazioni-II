RSpec.describe MmTracker::DisplayGenerator do

  before :example do
    @fixture_file = File.join(FIXTURE_PATH, 'features.yml')
  end

  it 'cannot be created' do
    expect { MmTracker::DisplayGenerator.new(@fixture_file) }.to raise_error(NoMethodError)
  end

end
