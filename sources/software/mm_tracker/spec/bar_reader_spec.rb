RSpec.describe MmTracker::BarReader do

  before :example do
    @file = File.join(FIXTURE_PATH, 'bars.txt')
    @bars = [1.02168, 3.43655, 5.94431, 8.35918]
    @eps = 1e-6
  end

  it 'can be created' do
    expect(MmTracker::BarReader.new(@file)).not_to be nil
  end

  it 'does read a bar file' do
    expect((mmt = MmTracker::BarReader.new(@file))).not_to be nil
    expect(mmt.bars).not_to be nil
    expect(mmt.bars.is_a?(Array)).to be true
    expect(mmt.bars.empty?).to be false
    @bars.each_index { |idx| expect(mmt.bars[idx]).to be_within(@eps).of(@bars[idx]) }
  end

end
