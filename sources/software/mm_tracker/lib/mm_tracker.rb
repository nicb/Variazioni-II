require "mm_tracker/version"

module MmTracker

  PATH = File.expand_path(File.join('..', 'mm_tracker'), __FILE__)

end

%w{
  reader
  beats_reader
  bar_reader
  display_generator
  beats_display_generator
  bar_display_generator
  plotter_generator
}.each { |f| require File.join(MmTracker::PATH, f) }
