#
# Sonic/graphic display generator for the features extracted with the essentia music extractor
#

module MmTracker

  class BeatsDisplayGenerator < DisplayGenerator

    public_class_method :new

    def initialize(f)
      @reader = BeatsReader.new(f)
    end

  end

end
