#
# Gnuplot display generator for the features extracted with the essentia music extractor
#

require 'erb'

module MmTracker

  class PlotterGenerator < BeatsDisplayGenerator

    def to_gnuplot
      res =  gnuplot_header
      res += gnuplot_data
      res += gnuplot_trailer
      res
    end

  private

    def gnuplot_data
      res = ''
      last = 0.0
      self.reader.elements.each do
        |bp|
        dur = bp-last
        mm  = 60.0/dur
        res += ("%12.9f %12.9f\n" % [ bp, mm ])
        last = bp
      end
      res
    end

    def gnuplot_header
      "set term x11\nsmooth bezier\nplot '-' with l\n"
    end

    def gnuplot_trailer
      "e\n"
    end

  end

end
