#
# This reads the bar files
#

module MmTracker

  class BarReader < Reader

    public_class_method :new

    alias_method :bars, :elements

  private

    def read
      bars = []
      lines = []
      File.open(self.file, 'r') { |f| lines = f.readlines }
      lines.each do
        |l|
        (time, dummy, barno) = l.split(/\s+/, 3)
        bars[barno.to_i] = time.to_f
      end
      bars
    end

  end

end
