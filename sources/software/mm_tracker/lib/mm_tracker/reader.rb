#
# Base class for the readers
#

require 'yaml'

module MmTracker

  class Reader

    private_class_method :new

    attr_reader :file, :elements

    def initialize(fn)
      @file = fn
      @elements = read
    end

  private

    class PureVirtualFunction < StandardError; end

    def read
      raise PureVirtualFunction
    end

  end

end
