#
# Sonic/graphic bar display generator for the features extracted with the essentia music extractor
#

require 'erb'

module MmTracker

  class BarDisplayGenerator < DisplayGenerator

    public_class_method :new

    def initialize(f)
      @reader = BarReader.new(f)
    end

  end

end
