#
# Base class for Sonic/graphic display generator for the features extracted with the essentia music extractor
#

require 'erb'

module MmTracker

  class DisplayGenerator

    attr_reader :reader

    private_class_method :new

    CSOUND_CSD_TEMPLATE = File.expand_path(File.join(['..'] * 3, 'templates', 'csound', 'mm_tracker.csd.erb'), __FILE__)

    def to_csound
      template = File.open(CSOUND_CSD_TEMPLATE, 'r') { |fh| fh.readlines.join }
      render = ERB.new(template)
      render.result(binding)
    end

  protected

    CSOUND_DUR = 0.005
    CSOUND_AMP = -1

    def to_csound_score
      res = csound_score_header
      self.reader.elements.each do
        |bp|
        res += ("i1 %12.9f %12.9f %+4.1f\n" % [ bp, CSOUND_DUR, CSOUND_AMP ])
      end
      res += csound_score_trailer
      res
    end

  private

    def csound_score_header
      "f1 0 16384 11 1\n"
    end

    def csound_score_trailer
      "e\n"
    end

  end

end
