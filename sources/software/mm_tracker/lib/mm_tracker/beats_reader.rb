#
# Yaml reader of the features extracted with the essentia music extractor
#

require 'yaml'

module MmTracker

  class BeatsReader < Reader

    public_class_method :new

    alias_method :beats_position, :elements

  private

    def read
      yaml_hash = YAML.load(File.open(self.file, 'r'))
      yaml_hash['rhythm']['beats_position']
    end

  end

end
