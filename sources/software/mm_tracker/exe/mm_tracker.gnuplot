#!/usr/bin/env ruby
$: << File.expand_path(File.join(['..'] * 2, 'lib'), __FILE__)

require 'mm_tracker'

feature_file = ARGV[0]
pg = MmTracker::PlotterGenerator.new(feature_file)
puts pg.to_gnuplot
