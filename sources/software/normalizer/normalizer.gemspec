# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'normalizer/version'

Gem::Specification.new do |spec|
  spec.name          = "normalizer"
  spec.version       = Normalizer::VERSION
  spec.authors       = ["Nicola Bernardini"]
  spec.email         = ['nicb@sme-ccppd.org']

  spec.summary       = %q{duration/pitch normalizer for the Variazioni II piece}
  spec.description   = %q{duration/pitch normalizer for the Variazioni II piece}
  spec.homepage      = %q{https://gitlab.com/nicb/Variazioni-II}

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
          spec.metadata['allowed_push_host'] = spec.homepage
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.9'
  spec.add_development_dependency 'rake', '~> 12.3'
  spec.add_development_dependency 'rspec'
end
