require 'spec_helper'

class A

  def method_1
    'I am method 1'
  end

  def method_2
    'I am method 2'
  end

  def method_3
    "23.23"
  end

end

class B

  include Delegate

  attr_reader :a

  def initialize
    @a = A.new
  end

  delegate :a, :method_1
  delegate :a, :method_2, as: :renamed_method
  delegate :a, :method_3, filter: :to_f

end

describe 'Delegate::delegate class method' do

  before :example do
    @b = B.new
    @m1_result = 'I am method 1'
    @rm_result = 'I am method 2'
    @m3_result = 23.23
  end

  it 'has the method_1, renamed_method and method_3 methods' do
    [:method_1, :renamed_method, :method_3].each { |m| expect(@b.respond_to?(m)).to be true }
  end

  it 'has a method_1 method that works correctly' do
    expect(@b.method_1).to eq('I am method 1')
  end

  it 'has a renamed_method method that works correctly' do
    expect(@b.renamed_method).to eq('I am method 2')
  end

  it 'has a method_3 method that works correctly' do
   expect(@b.method_3.is_a?(@m3_result.class)).to be true
   expect(@b.method_3).to eq(@m3_result)
  end


end
