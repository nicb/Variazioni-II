require 'spec_helper'

describe Normalizer::Segment do

  before :example do
    @song = Normalizer::Song.new('IT')
    @seg = @song.segments.first
    @init_dur = @seg.dur
    @final_dur = @seg.normalized_dur
    @tfvalue = @init_dur / @final_dur
  end

  it 'can be created' do
    expect(@seg).not_to be nil
  end

  it 'has a dur() instance method' do
    expect(@seg.respond_to?(:dur)).to be true
  end

  it 'has a dur() instance method that works' do
    expect(@seg.dur).to eq(@init_dur)
  end

  it 'has a time_factor()/time_factor=() instance method' do
    expect(@seg.respond_to?(:time_factor)).to be true
    expect(@seg.respond_to?(:time_factor=)).to be true
  end

  it 'has a time_factor()/time_factor=() instance method that works' do
    @seg.time_factor = @tfvalue
    expect(@seg.time_factor).to eq(@tfvalue)
  end

  it 'has a normalize() instance method' do
    expect(@seg.respond_to?(:normalize)).to be true
  end

  it 'has a normalize() instance method that works' do
    ns = @seg.clone
    ns.normalize(@final_dur)
    expect(ns.time_factor).to eq(@tfvalue)
  end

  it 'has a normalized_dur() instance method' do
    expect(@seg.respond_to?(:normalized_dur)).to be true
  end

  it 'has a normalized_dur() instance method that works' do
    ns = @seg.clone
    ns.normalize(@final_dur)
    expect(ns.time_factor).to eq(@tfvalue)
    expect(ns.normalized_dur).to eq(@final_dur)
  end

  it 'has a csound_dur() instance method' do
    expect(@seg.respond_to?(:csound_dur)).to be true
  end

  it 'has a csound_dur() instance method that works' do
    ns = @seg.clone
    @result_should_be = (ns.dur / ns.song.orig_sinc).round
    expect(ns.csound_dur).to eq(@result_should_be)
  end

  it 'has a csound_warping() instance method' do
    expect(@seg.respond_to?(:csound_warping)).to be true
  end

  it 'has a csound_warping() instance method that works' do
    ns = @seg.clone
    ns.normalize(@final_dur)
    @result_should_be = ns.time_factor
    expect(ns.csound_warping).to eq(@result_should_be)
  end

  it 'has a grid_vertical_position() instance method' do
    expect(@seg.respond_to?(:grid_vertical_position)).to be true
  end

  it 'has a grid_vertical_position() instance method that works', :slow do
    ns = @seg.clone
    ns.normalize(@final_dur)
    result_should_be = 1.1102230246251565e-16
    expect(ns.grid_vertical_position).to eq(result_should_be)
  end

end
