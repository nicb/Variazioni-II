require 'spec_helper'

describe Normalizer do
  it 'has a version number' do
    expect(Normalizer::VERSION).not_to be nil
  end
end

describe Normalizer::Normalizer do

  before :example do
    @n = Normalizer::Normalizer.new
    @dtf = 1.0 # default time factor
    @l = 'IT'
  end

  it 'can be created' do
    expect(@n).not_to be nil
  end

  it 'contains the exact number of songs once created' do
    expect(@n.songs.size).not_to eq(0)
    expect(@n.songs.size).to eq(Normalizer::SOURCES_DIRS.size)
  end

  it 'has a to_pic() instance method' do
    expect(@n.respond_to?(:to_pic)).to be true
  end

  it 'has a to_pic() instance method that works', :slow do
    expect(@n.to_pic).not_to be nil
    expect(@n.to_pic.empty?).to be false
  end

  it 'has a find_song() private method that works' do
     expect(res = @n.send(:find_song, @l)).not_to be nil
     expect(res.language).to eq(@l)
  end

end
