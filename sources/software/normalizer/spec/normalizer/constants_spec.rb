require 'spec_helper'

describe 'Normalizer::constants' do

  before :example do
    @excluded_dirs = %w(README.md software).map { |f| File.join(Normalizer::SOURCES_FC, f) }
  end

  it 'has a SOURCES_ROOT variable' do
    expect(Normalizer::SOURCES_ROOT).not_to be nil
  end

  it 'has a SOURCES_ROOT variable that is properly configured' do
    expect(Dir.exists?(Normalizer::SOURCES_ROOT)).to be true
  end

  it 'has a SOURCES_FC variable' do
    expect(Normalizer::SOURCES_FC).not_to be nil
  end

  it 'has a SOURCES_FC variable that is properly configured' do
    expect(Dir.exists?(Normalizer::SOURCES_FC)).to be true
  end

  it 'has a SOURCES_EXCLUDE variable' do
    expect(Normalizer::SOURCES_EXCLUDE).not_to be nil
  end

  it 'has a SOURCES_EXCLUDE variable that is properly configured' do
    @excluded_dirs.each { |d| expect(File.exists?(d)).to(be(true), d) }
  end

  it 'has a SOURCES_DIRS variable' do
    expect(Normalizer::SOURCES_DIRS).not_to be nil
  end

  it 'has a SOURCES_DIRS variable that is properly configured' do
    Normalizer::SOURCES_DIRS.each { |d| expect(Dir.exists?(d)).to(be(true), d) }
    @excluded_dirs.each { |d| expect(Normalizer::SOURCES_DIRS.include?(d)).to(be(false), d) }
  end

end
