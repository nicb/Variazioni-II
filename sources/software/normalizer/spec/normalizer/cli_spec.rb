require 'fileutils'

describe 'normalizer.csound CLI' do

  before :example do
    @l = 'IT'
    @fc_dir = File.expand_path(File.join(['..'] * 5, 'first_conditioning', @l), __FILE__)
    @sco_file = File.join(@fc_dir, 'normalize.sco')
    @command_line="./exe/normalizer.csound #{@l}"
  end

  after :example do
    FileUtils.rm_f(@sco_file)
  end

  it 'does its job' do
    expect(system(@command_line)).to be true
    expect(File.exist?(@sco_file)).to be true
  end

end

describe 'normalizer.pic CLI' do

  before :example do
    @command_line="./exe/normalizer.pic"
  end

  it 'apparently does its job' do
    expect((n = Normalizer::Normalizer.new)).not_to be nil
    expect((out = n.to_pic).empty?).to be false
    expect(out).to match(/^.PS$/)
    expect(out).to match(/^.PE$/)
  end

  it 'really does its job' do
    IO::popen(@command_line, 'r') do
      |io|
      lines = io.readlines.join
      expect(lines).to match(/^.PS$/)
      expect(lines).to match(/^.PE$/)
    end
  end

end
