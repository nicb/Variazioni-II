describe Normalizer::Tuning do

  class Tuned
    attr_reader :key, :tuning

    include Normalizer::Tuning

    def initialize(k, t)
      @key = k
      @tuning = t
    end

  end

  before :example do
    @ref_key = 'A#'
    @ref_tuning = 448.726654053
    @a_tuning = 439
    @a_key = 'G#'
    @a = Tuned.new(@a_key, @a_tuning)
    @a_retune_should_be = 1.147331752
    @b_tuning = 449
    @b_key = 'B'
    @b = Tuned.new(@b_key, @b_tuning)
    @b_retune_should_be = 0.94330
    @far_tuning = 435.700531006
    @far_key = 'E'
    @far = Tuned.new(@far_key, @far_tuning)
    @far_retune_should_be = 1.091137824
    @eps = 1e-6
  end

  it "calculates the retuning properly for key #{@a_key} and tuning #{@a_tuning}" do
    expect(@a.retune).to be_within(@eps).of @a_retune_should_be
  end

  it "calculates the retuning properly for key #{@b_key} and tuning #{@b_tuning}" do
    expect(@b.retune).to be_within(@eps).of @b_retune_should_be
  end

  it "calculates the retuning properly for key #{@far_key} and tuning #{@far_tuning}" do
    expect(@far.retune).to be_within(@eps).of @far_retune_should_be
  end

end
