require 'spec_helper'

describe Normalizer::ReferenceSong do

  before :example do
    @rs = Normalizer::REFERENCE_SONG
    @num_segments = 32 # there are 32 bars
  end

  it 'cannot be created' do
    expect { ReferenceSong.new('IT') }.to raise_error(NameError)
  end

  it 'does exist anyway' do
    expect(@rs).not_to be nil
    expect(@rs.class).to be(Normalizer::ReferenceSong)
  end

  it 'has a language() instance method' do
    expect(@rs.respond_to?(:language)).to be true
  end

  it 'has a language() instance method that works' do
    expect(@rs.language).to eq(Normalizer::ReferenceSong::REFERENCE_LANGUAGE)
  end

  it 'has a segments() instance method' do
    expect(@rs.respond_to?(:segments)).to be true
  end

  it 'actually has segments' do
    expect(@rs.segments.size).to eq(@num_segments)
    @rs.segments.each { |s| expect(s.class).to eq(Normalizer::Segment) }
  end

end

