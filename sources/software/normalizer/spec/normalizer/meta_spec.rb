require 'spec_helper'

describe Normalizer::Meta do

  before :example do
    @sample_label = 'NO'
    @m = Normalizer::Meta.new(@sample_label)
    hash = YAML.load(File.open(@m.send(:meta_path), 'r+'))
    @sample_tuning = hash['tonal']['tuning_frequency']
    @sample_key = hash['tonal']['key_edma']['key'].to_s
    @sample_beats_position = hash['rhythm']['beats_position']
    @sample_duration = 92.8234939575
    @sample_amplitude = -6.90776824951
    @eps = 1e-4
  end

  it 'can be created' do
    expect(@m).not_to be nil
  end

  it 'has a language() instance method that works' do
    expect(@m.respond_to?(:language)).to be true
    expect(@m.language).to eq(@sample_label)
  end

  it 'has a tuning() instance method that works' do
    expect(@m.respond_to?(:tuning)).to be true
    expect(@m.tuning).to eq(@sample_tuning)
  end

  it 'has a key() instance method that works' do
    expect(@m.respond_to?(:key)).to be true
    expect(@m.key).to eq(@sample_key)
  end

  it 'has a beats_position() instance method that works' do
    expect(@m.respond_to?(:beats_position)).to be true
    expect(@m.beats_position).to eq(@sample_beats_position)
  end

  it 'has a duration() instance method that works' do
    expect(@m.respond_to?(:duration)).to be true
    expect(@m.duration).to eq(@sample_duration)
  end

  it 'has a amplitude() instance method that works' do
    expect(@m.respond_to?(:amplitude)).to be true
    expect(@m.amplitude).to be_within(@eps).of(@sample_amplitude)
  end

end
