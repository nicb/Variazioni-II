require 'spec_helper'

describe Normalizer::Reader do

  before :example do
    @sample_language = 'IT'
    @song = Normalizer::Song.new(@sample_language)
    @r = Normalizer::Reader.new(@song)
    @num_segments = 32 # songs are 32 bars long
    expect(File.exists?(@song.bars_filename)).to be true
  end

  it 'can be created' do
    expect(@r).not_to be nil
  end

  it 'has a read_form() instance method' do
    expect(@r.respond_to?(:read_form)).to be true
  end

  it 'has a read_bars() instance method' do
    expect(@r.respond_to?(:read_bars)).to be true
  end

  it 'has a read() class method' do
    expect(Normalizer::Reader.respond_to?(:read)).to be true
  end

  it 'actually reads and load bar segments' do
    @r.read_bars
    expect(@r.segments.size).to eq(@num_segments)
    @r.segments.each { |s| expect(s.class).to eq(Normalizer::Segment) }
  end

  it 'actually reads and load segments from the class method too' do
    r = Normalizer::Reader.read(@song)
    expect(r.segments.size).to eq(@num_segments)
    r.segments.each { |s| expect(s.class).to eq(Normalizer::Segment) }
  end

end
