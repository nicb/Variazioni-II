require 'spec_helper'

describe 'Octave::eval_to_f()' do

  before :example do
    x = 5.5; y = 25.25
    @simple_statement = "echo '#{x} * #{y}'"
    @simple_statement_result = (((x * y) * 100).round) / 100.0
    @scripts_file_path = File.expand_path(File.join('..', 'test_scripts'), __FILE__)
    @test0_path = File.join(@scripts_file_path, 'test0.m')
    @compound_statement = "(echo 'x = #{x};'; cat #{@test0_path})"
    @compound_statement_results = [@simple_statement_result, x, y]
  end

  it 'actually works with simple statements' do
    expect(Octave::eval_to_f(@simple_statement).first).to eq(@simple_statement_result)
  end

  it 'does work with compound statements too' do
    expect(res = Octave::eval_to_f(@compound_statement, /^[a-z]+\s+=\s+/)).not_to be nil
    @compound_statement_results.each_index { |idx| expect(res[idx]).to eq(@compound_statement_results[idx]) }
  end

end
