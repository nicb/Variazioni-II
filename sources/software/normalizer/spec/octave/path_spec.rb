require 'spec_helper'

describe 'Octave::path()' do

  before :example do
    @dir_should_be = Octave::SCRIPTS_PATH
    @existing_filename = 'pps_gnf.m'
    @full_path = File.join(@dir_should_be, @existing_filename)
    expect(File.exists?(@full_path)).to be true
  end

  it 'actually works' do
    expect(File.exists?(Octave::path(@existing_filename))).to be true
  end

end

