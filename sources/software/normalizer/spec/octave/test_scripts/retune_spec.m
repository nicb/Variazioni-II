%
% 'key' and 'tuning' should be passed via pipe
#
% key = 4
% tuning = 435.700531006
%
ref_tuning = 448.726654053;
ref_key    = 10;

retune_tuning = ref_tuning / tuning
retune_steps  = ref_key - key

if retune_steps > 6
   retune_steps -= 12;
end

if retune_steps < -6
   retune_steps += 12;
end

if retune_steps > 4
   retune_steps -= 5;
end

if retune_steps < -4
   retune_steps += 7;
end

retune_steps

retune_key    = 2**(retune_steps/12)
retune = retune_tuning * retune_key;
printf("retune = %14.9f\n", retune);
