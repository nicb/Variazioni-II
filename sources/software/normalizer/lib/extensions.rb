module Normalizer

  EXTPATH= File.expand_path(File.join('..', 'extensions'), __FILE__)

end

%w(
  delegate
).each { |f| require File.join(Normalizer::EXTPATH, f) }
