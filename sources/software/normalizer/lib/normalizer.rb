require 'bundler/setup'

module Normalizer

  LIBPATH= File.expand_path(File.join('..', 'normalizer'), __FILE__)

end

require File.join(Normalizer::LIBPATH, 'version')
require File.join(Normalizer::LIBPATH, 'constants')

%w(
  constants
  segment
  display
  reader
  tuning
  meta
  song
  reference_song
  normalizer
).each { |f| require File.join(Normalizer::LIBPATH, f) }
