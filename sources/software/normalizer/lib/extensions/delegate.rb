module Delegate

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods

    #
    # <tt>delegate(obj, meth, options = {})</tt>
    #
    # simple bare-bones delegation: defines an instance method +meth+
    # which delegates an internal object +obj+ to run it. 
    # Can optionally use a set of options:
    #
    # * :filter    => filter method with some conditioning method (such as
    #                 +:to_f+, +:to_i+, etc.)
    # * :as        => rename method to respond to a different name
    #
    # PLEASE NOTE: +obj+ must be a symbol for a method in the receiver instance
    #              that actually returns the delegated object
    #
    def delegate(obj, meth, options = {})
      opts = normalize_options(options)
      mname = opts[:as] ? opts[:as] : meth
      self.send(:define_method, mname) do
        res = send(obj).send(meth)
        res = res.send(opts[:filter]) if opts[:filter]
        res
      end
    end

  private

    def normalize_options(opts)
      res = { filter: nil, as: nil }
      opts.each { |k, v| res[k] = v }
      res
    end

  end

end
