%
% pic pvoc speed grid normalization function (octave)
%
%
% conditions are:
%
% when x0 = 0.1 then y0 = -0.5
% when x1 = 1.0 then y1 =  0.0
% when x2 = 1.9 then y2 =  0.5
%
% equation is: y = a * x^2 + b * x + c
%
xaxis = [-0.1:0.0001:2.1];
x0 = 0.1;
x1 = 1.0;
x2 = 1.9;
y0 = -0.5;
y1 = 0.0;
y2 = 0.5;
P = polyfit([x0 x1 x2], [y0 y1 y2], 2);
% yaxis = polyval(P, xaxis);

% plot(xaxis, yaxis)

a = P(1); b = P(2); c = P(3);
% printf("a: %35.33f, b: %35.33f, c: %35.33f\n", a, b, c);
y = (a * (x^2)) + (b * x) + c;
printf("ans = %35.33f\n", y);
