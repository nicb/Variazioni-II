module Octave

   class << self

     def path(filename)
       File.join(SCRIPTS_PATH, filename)
     end

   end

end
