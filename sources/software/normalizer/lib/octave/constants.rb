module Octave

  EXECUTABLE= File.join('', 'usr', 'bin', 'octave-cli')
  OPTIONS = '-q'
  SCRIPTS_PATH = File.join(LIBPATH, 'scripts')

end
