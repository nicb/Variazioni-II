module Octave

   class << self

     def eval_to_f(eval_string, prefix = '^ans\s+=\s+')
       f = IO.popen("#{eval_string} | #{EXECUTABLE} #{OPTIONS}", "r+")
       lines = f.readlines
       res = lines.map { |l| l.chomp.sub(/#{prefix}/, '').to_f }
       f.close
       res
     end

   end

end
