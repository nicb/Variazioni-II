require 'octave'

module Normalizer

  class Segment
    attr_reader :start, :stop, :label, :song
    attr_accessor :time_factor

    def initialize(strt, stp, l, sng)
      @start = strt
      @stop = stp
      @label = l
      @song = sng
      self.time_factor = 1.0
    end

    def conditioned_label
      self.song.language + '_' + self.label.gsub(/\W/, '_')
    end

    def dur
      self.stop - self.start
    end

    def freq
       1.0 / self.dur
    end

    def normalize(ref_seg)
      self.time_factor = self.dur / ref_seg
    end

    def normalized_dur
      self.dur / self.time_factor
    end

    def normalized_freq
       1.0 / self.normalized_dur
    end

    def samples
      (self.dur / self.song.orig_sinc).round
    end

    def normalized_samples
      (self.normalized_dur / self.song.final_sinc).round
    end
    #
    # <tt>csound_dur()</tt>
    # dur in gen table samples
    #
    alias_method :csound_dur, :samples

    #
    # <tt>csound_warping</tt>
    # frequency warping required to normalize segment.
    # Returns the actual final frequency.
    #
    alias_method :csound_warping, :time_factor

    #
    # +grid_vertical_position+:
    #
    # returns the vertical position in the pic pvoc grid
    # (used only for display)
    #
    # This calculation is very taxing so the value gets cached the first time.
    #
    def grid_vertical_position
      return @gvpos if @gvpos
      cmd = "(echo 'x = #{self.csound_warping};'; cat #{Octave::path('pps_gnf.m')})"
      @gvpos = Octave::eval_to_f(cmd).first
    end

  end

end
