require 'singleton'

module Normalizer

  class ReferenceSong < Song

    include Singleton

    REFERENCE_LANGUAGE = 'RU'

    def initialize
      super(REFERENCE_LANGUAGE)
    end

  end

  REFERENCE_SONG = ReferenceSong.instance

end
