require 'vII/common'

module Normalizer

  SOURCES_ROOT = File.expand_path(File.join(['..'] * 5), __FILE__)
  SOURCES_FC = File.join(SOURCES_ROOT, 'first_conditioning')
  SOURCES_EXCLUDE = %w(README.md Make.include software Makefile mfe-profile.yaml).map { |f| File.join(SOURCES_FC, f) }
  SOURCES_DIRS = VII::Common::LANGUAGES.map { |l| File.join(SOURCES_FC, l) }

end
