require 'yaml'

module Normalizer

  class Reader

    attr_reader :segments, :song

    def initialize(sng)
      @song = sng
      @segments = []
    end

    class <<self

      def read(song)
#       read_form(song)
#       read_beats(song)
        read_bars(song)
      end

      def read_form(song)
        r = new(song)
        r.read_form
        r
      end

      def read_beats(song)
        r = new(song)
        r.read_beats
        r
      end

      def read_bars(song)
        r = new(song)
        r.read_bars
        r
      end

    end

    def read_form
      File.open(self.song.labels_filename, 'r') do
        |fh|
        while(!fh.eof?)
          line = fh.gets.chomp
          (start, stop, label) = line.split(/\s+/, 3)
          start = start.to_f
          stop  = stop.to_f
          self.segments << Segment.new(start, stop, label, self.song)
        end
      end
      self.segments.compact!
      self
    end

    def read_beats
      idx = 0
      while(idx < self.song.beats_position.size-1)
        start = self.song.beats_position[idx]
        stop  = self.song.beats_position[idx+1]
        label = "b#{idx}"
        self.segments << Segment.new(start, stop, label, self.song)
        idx += 1
      end
      self.segments.compact!
      self
    end

    def read_bars
      File.open(self.song.bars_filename, 'r') do
        |fh|
        last = 0
        while(!fh.eof?)
          line = fh.gets.chomp
          (at, dummy, barno) = line.split(/\s+/, 3)
          start = last
          stop  = at.to_f
          label = barno.to_s
          self.segments << Segment.new(start, stop, label, self.song)
          last = stop
        end
      end
      self.segments.compact!
      self
    end

  end

end
