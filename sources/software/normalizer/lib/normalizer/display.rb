module Normalizer
  module Display

    LIBPATH= File.join(Normalizer::LIBPATH, 'display')

  end
end

%w(
  pic
  csound
  octave
).each { |f| require File.join(Normalizer::Display::LIBPATH, f) }
