require 'extensions'

module Normalizer

  class Song
    attr_reader :language

    GAIN_RESCALING_FACTOR = 3.0 # amplitude attenuation rescaling

    include Delegate

    delegate :metadata, :language
    delegate :metadata, :key
    delegate :metadata, :tuning, filter: :to_f
    delegate :metadata, :beats_position
    delegate :metadata, :amplitude

    def initialize(l)
      @_meta_ = Meta.new(l)
      @language = l
      @normalized = false
      @reader = Reader.read(self)
    end

    def gain
      self.amplitude / GAIN_RESCALING_FACTOR
    end

    def segments
      @reader.segments
    end

    def dur
      common_dur { |s| s.dur }
    end

    def normalized_dur
      normalize! unless normalized?
      common_dur { |s| s.normalized_dur }
    end

    def to_pic
      normalize! unless normalized?
      self.class.include Display::Pic::Song
      display
    end

    def to_csound
      normalize! unless normalized?
      self.class.include Display::Csound::Score
      res = generate_score
    end

    def to_octave
      normalize! unless normalized?
      self.class.include Display::Octave::Song
      display
    end

    #
    # +orig_sinc+
    #
    # csound sample increment, that is the full original duration of
    # the song divided by the number of GEN samples
    #
    # This value is cached for multiple references
    #
    def orig_sinc
      @orig_sinc = self.dur / Display::Csound::PVOC_SPEED_GEN_TABLE_SIZE.to_f unless @orig_sinc
      @orig_sinc
    end

    #
    # +final_sinc+
    #
    # csound sample increment, that is the full normalized duration of
    # the song divided by the number of GEN samples
    #
    # This value is cached for multiple references
    #
    def final_sinc
      @final_sinc = reference_dur / Display::Csound::PVOC_SPEED_GEN_TABLE_SIZE.to_f unless @final_sinc
      @final_sinc
    end

    def labels_filename
      File.join(SOURCES_FC, self.language, "#{self.language}_labels.txt")
    end

    def bars_filename
      File.join(SOURCES_FC, self.language, "#{self.language.downcase}-bars.txt")
    end

    include Tuning

  private

    def metadata
      @_meta_
    end

    def reference_dur
      REFERENCE_SONG.dur
    end

    def common_dur
      res = 0.0
      self.segments.each { |s| res += yield(s) }
      res
    end

    def normalized?
      @normalized
    end

    def normalize!
      rs = REFERENCE_SONG
      self.segments.each_index do
        |idx|
        self.segments[idx].normalize(rs.segments[idx].dur) if rs.segments[idx]
      end
    end

  end

end
