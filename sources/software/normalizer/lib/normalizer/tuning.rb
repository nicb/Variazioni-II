require 'yaml'

module Normalizer
  module Tuning

    #
    # FIXME: most of this code is useless, because we no longer use Essentia
    # to extract key and tuning (we have done this by ear using the puredata
    # +tuner.pd+ patch provided in the ../puredata folder). 
    # Useless code will be removed as soon as we are certain that it won't be
    # of use any longer.
    #
    def retune
#     extract_tuning_in_one_way_or_another
      k = self.key
      t = self.tuning
      key_normalization_factor(k) * tuning_normalization_factor(t)
    end

  private

    #
    # simple pitch-class extraction
    #
    KEYS = { 'C' => 0, 'B#' => 0,
             'C#' => 1, 'Db' => 1,
             'D' => 2,
             'D#' => 3, 'Eb' => 3,
             'E' => 4,
             'F' => 5, 'E#' => 5,
             'F#' => 6, 'Gb' => 6,
             'G' => 7,
             'G#' => 8, 'Ab' => 8,
             'A' => 9,
             'A#' => 10, 'Bb' => 10,
             'B' => 11, }


    def key_normalization_factor(k)
      myk = KEYS[k]
      refk = KEYS[REFERENCE_SONG.key]
      semi = refk - myk
      #
      # intervals should fall within a major third in order
      # to be credible, so we:
      # - add or subtract an octave if we're within an augmented fourth
      # - add a fifth or subtract a fourth if we're still over a major third
      #
      semi += 12 if semi < -6
      semi -= 12 if semi > +6
      semi +=  7 if semi < -4
      semi -=  5 if semi > +4
      2**(semi.to_f/12.0)
    end

    def tuning_normalization_factor(t)
      reft = REFERENCE_SONG.tuning
      reft / t
    end

#   #
#   # <tt>extract_tuning_in_one_way_or_another()</tt>
#   #
#   # extracts key and tuning frequency data from the mono version of the tune
#   # either with Essentia or, if Essentia did not produce the expected
#   # results, through a metadata file produced by hand.
#   #
#   # In order to work this module needs:
#   # * the Essentia library to be
#   #   installed and functional and in particular it needs the
#   #   +streaming_key+ cli app loaded in the
#   #   <tt>/usr/local/bin/essentia</tt> path
#   # * a <tt><language>-mono.wav</tt> to be existing in the directory
#   #

#   TUNING_APP = '/usr/local/bin/essentia/streaming_key'
#   MONO_FILE_SFX = '-mono.wav'
#   YAML_ESSENTIA_FILE_SFX = '-key.yaml'
#   YAML_META_FILE_SFX = '-meta.yaml'

#   class ExtractTuningError < StandardError; end

#   def extract_tuning_in_one_way_or_another
#     return @__retune__ if @__retune__
#     return extract_tuning_from_file if has_tuning_file?
#     extract_tuning_with_essentia
#   end

#   def has_tuning_file?
#     File.exists?(meta_file_path) && !extract_tuning_from_file.nil?
#   end

#   def extract_tuning_from_file
#     return @__retune__ if @__retune__
#     File.open(meta_file_path, 'r+') do
#       |fh|
#       y = YAML.load(fh)
#       @__retune__ = y['metadata']['retune']
#     end
#     @__retune__
#   end
#   
#   def extract_tuning_with_essentia
#     return @__retune__ if @__retune__
#     unless File.exists?(essentia_file_path)
#       cli = "#{TUNING_APP} #{mono_file_path} #{essentia_file_path} 2>/dev/null 1>&2"
#       sout = system(cli)
#       raise ExtractTuningError, cli unless sout
#     end
#     m = YAML.load(File.open(essentia_file_path, 'r+'))
#     key = m['tonal']['key']
#     tuning_freq = m['tonal']['tuning_freq']['mean']
#     knf = key_normalization_factor(key)
#     tnf = tuning_normalization_factor(tuning_freq)
#     @__retune__ = knf * tnf
#   end

#   def file_path(sfx)
#     f = self.language.downcase + sfx
#     File.join(SOURCES_FC, self.language, f)
#   end

#   def essentia_file_path
#     file_path(YAML_ESSENTIA_FILE_SFX)
#   end

#   def mono_file_path
#     file_path(MONO_FILE_SFX)
#   end

#   def meta_file_path
#     file_path(YAML_META_FILE_SFX)
#   end

  end
end
