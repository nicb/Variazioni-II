require 'yaml'

module Normalizer

  class Meta

    attr_reader :language, :key, :tuning, :beats_position, :duration, :amplitude

    def initialize(l)
      @language = l
      load_meta
    end

  private

    def load_meta
      hash = YAML.load(File.open(meta_path, 'r+'))
      @key = meta_key(hash)
      @tuning = meta_tuning(hash)
      @beats_position = meta_beats_position(hash)
      @duration = meta_duration(hash)
      @amplitude = meta_amplitude(hash)
    end

    def meta_path
      File.join(SOURCES_FC, self.language, "#{self.language.downcase}-features.yml")
    end

    def meta_key(hash)
      hash['tonal']['key_edma']['key']
    end

    def meta_tuning(hash)
      hash['tonal']['tuning_frequency'].to_f
    end

    def meta_beats_position(hash)
      hash['rhythm']['beats_position']
    end

    def meta_duration(hash)
      hash['metadata']['audio_properties']['length'].to_f
    end

    def meta_amplitude(hash)
      hash['metadata']['audio_properties']['replay_gain'].to_f
    end

  end

end
