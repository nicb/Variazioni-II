module Normalizer
  module Display

    module Octave

      module Gen

        #
        # <tt>gen_to_octave(lh = 'ifun')
        # +gen_to_octave+ organizes a function that is
        # +PVOC_SPEED_GEN_TABLE_SIZE+ big and which holds the reference time
        # on its x axis and the original time on its y axis. 
        # The original time is calculated in seconds while the reference time
        # gets calculated in samples
        #
        def gen_to_octave(lh = 'ifun')
          tabsize = ::Normalizer::Display::Csound::PVOC_SPEED_GEN_TABLE_SIZE
          res = "#{lh} = zeros(1,#{tabsize});\n"
          from = 1
          yval = 0.0
          self.segments.each do
            |cur|
            (to, str) = gen_to_octave_core(lh, from, cur, yval)
            res += str
            from = to
            yval += cur.dur
          end
          res
        end

      private

        def gen_to_octave_core(lh, from, cur, yval)
          samples = cur.normalized_samples
          sinc = cur.dur.to_f / samples
          to = from + samples
          s = ("#{lh}(%d:%d) = [%20.17f:%+20.17f:%20.17f-(%+20.17f)];\n"  % [from, to-1, yval, sinc, yval + cur.dur, sinc])
          [to, s]
        end

      end

    end

  end
end
