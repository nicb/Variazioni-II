require 'erb'

module Normalizer
  module Display

    module Octave

      VN_NAME       = 'verify_normalization.m'
      TEMPLATE_PATH = File.expand_path(File.join('..', 'templates'), __FILE__)
      VN_ERB_PATH   = File.join(TEMPLATE_PATH, "#{VN_NAME}.erb")
      LOAD_PATH     = File.expand_path(File.join(['..'] * 7, 'first_conditioning', 'software', 'octave'), __FILE__)

      module Song

        include Gen

        def display
          b = binding
          erb = ERB.new(File.read(verify_normalization_template_path))
          erb.result(b)
        end

      private

        def verify_normalization_template_path
          ::Normalizer::Display::Octave::VN_ERB_PATH
        end

      end

    end

  end
end
