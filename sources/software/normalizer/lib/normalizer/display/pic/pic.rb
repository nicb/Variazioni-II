require 'erb'

module Normalizer
  module Display

    module Pic

      TEMPLATE_PATH = File.expand_path(File.join('..', 'templates'), __FILE__)
      HEADER_PATH   = File.join(TEMPLATE_PATH, 'header.pic')
      HEADER_TEMPLATE_PATH   = File.join(TEMPLATE_PATH, 'header.pic.erb')
      SONG_PATH     = File.join(TEMPLATE_PATH, 'song.pic.erb')

      module Song

        def display
          b = binding
          erb = ERB.new(File.read(song_path))
          erb.result(b)
        end

      private

        def song_path
          ::Normalizer::Display::Pic::SONG_PATH
        end

      end

      module Environ

        def header
          b = binding
          erb = ERB.new(File.read(header_template_path))
          erb.result(b)
        end

        def trailer
          ''
        end

      private

        def header_template_path
          ::Normalizer::Display::Pic::HEADER_TEMPLATE_PATH
        end

        def header_path
          ::Normalizer::Display::Pic::HEADER_PATH
        end

      end

    end

  end
end
