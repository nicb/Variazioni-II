module Normalizer
  module Display
    module Pic

      LIBPATH= File.join(Normalizer::Display::LIBPATH, 'pic')

    end
  end
end

%w(
  pic
).each { |f| require File.join(Normalizer::Display::Pic::LIBPATH, f) }
