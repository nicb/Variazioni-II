module Normalizer
  module Display
    module Octave

      LIBPATH= File.join(Normalizer::Display::LIBPATH, 'octave')

    end
  end
end

%w(
  gen
  octave
).each { |f| require File.join(Normalizer::Display::Octave::LIBPATH, f) }
