module Normalizer
  module Display
    module Csound

      LIBPATH= File.join(Normalizer::Display::LIBPATH, 'csound')

    end
  end
end

%w(
  constants
  gen
  score
).each { |f| require File.join(Normalizer::Display::Csound::LIBPATH, f) }
