module Normalizer
  module Display

    module Csound

      PVOC_SPEED_GEN_TABLE_SIZE = 2**14
      PVOC_SPEED_INTERPOLATION  = 2     # 2 interpolation samples between one speed and the next
      NORMALIZER_SCORE_FILENAME = 'normalize.sco'

    end

  end
end
