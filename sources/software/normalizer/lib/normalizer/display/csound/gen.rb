module Normalizer
  module Display

    module Csound

      module Gen

        def generate_gen_line
          res = "f1 0 #{::Normalizer::Display::Csound::PVOC_SPEED_GEN_TABLE_SIZE} -7 "
          yval = 0.0
          self.segments.each do
            |s|
            res += ("%20.17f %4d " % [ yval, s.normalized_samples ])
            yval += s.dur
          end
          res += ("%20.17f\n" % [ yval ])
          res
        end

      end

    end

  end
end
