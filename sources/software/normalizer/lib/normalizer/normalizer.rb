module Normalizer

  class Normalizer
    attr_reader :songs

    def initialize
      @songs = load_songs
    end

    def to_pic
      self.class.include Display::Pic::Environ
      res = header
      res += self.songs.map { |s| s.to_pic }.join
      res += trailer
      res
    end

    def to_csound(language = nil)
      res = nil
      if language
        res = to_csound_one(language)
      else
        res = to_csound_all
      end

    end

  private

    def load_songs
      res = []
      stl = songs_to_load
      stl.each { |l| res << Song.new(l) }
      res
    end

    def songs_to_load
      res = SOURCES_DIRS.map { |d| File.basename(d) }
    end

    def find_song(l)
      res = nil
      self.songs.each do
        |s|
        if s.language == l
           res = s
           break
        end
      end
      res
    end

    class SongNotFound < StandardError; end

    def to_csound_one(l)
      res = nil
      s = find_song(l)
      raise SongNotFound, l unless s
      to_csound_output(s)
    end

    def to_csound_all
      self.songs.each { |s| to_csound_output(s) }
    end

    def to_csound_output(s)
      #
      # csound score
      #
      to_common_output(s, ::Normalizer::Display::Csound::NORMALIZER_SCORE_FILENAME) { |s| s.to_csound }
      #
      # octave verification file
      #
      to_common_output(s, ::Normalizer::Display::Octave::VN_NAME) { |s| s.to_octave }
    end

    def to_common_output(s, fname)
      res = nil
      path = File.join(SOURCES_FC, s.language)
      filename = File.join(path,  fname)
      File.open(filename, 'w') do
        |fh|
        res = yield(s)
        fh.puts res
      end
      res
    end

  end

end
