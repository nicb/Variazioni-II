module Octave

  LIBPATH= File.expand_path(File.join('..', 'octave'), __FILE__)

end

require File.join(Octave::LIBPATH, 'constants')

%w(
  path
  eval
).each { |f| require File.join(Octave::LIBPATH, f) }

