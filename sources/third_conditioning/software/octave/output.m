%
% output section of the stretcher function
%

figure(1, "visible", "off")
plot(T, fun, VII_X_COORDS, I_Y_COORDS, '+');
set(gca, "xtick", [0:50:VII_DUR+200]);
set(gca, "ytick", [0:10:I_END]);
grid on;
print("stretch_function.jpg", "-djpeg");
rotated_fun = fun';
save "-text" "stretch_function.inc" rotated_fun

%
% save some constants to help csound
%
fh = fopen("constants.inc", "w");
fprintf(fh, "gitotdur    = %10.7f\n", VII_DUR);
fflush(fh);
fclose(fh);

%
% build the pvoc score code line to help csound
%
fh = fopen("pvoc_score_line.inc", "w");
fprintf(fh, "i1 0   %10.7f %10.7f\n", VII_DUR, I_END);
fflush(fh);
fclose(fh);
