%
% constants for the stretcher function
%
% abstract numbers
%
PHI = 0.6180339; % the famous magic number
LEE = 0.5;       % 0.5 seconds leeway for curves
%
% Internationale (input) section
%
I_START = 1;
I_PHI = PHI**1.5;
I_BRIDGE_POINT = 19.199; % where the bridge really is
I_ACCEL1_POINT = I_BRIDGE_POINT * (PHI**2);
I_THREE_CLIMAX_NOTES_DUR = 1.715; % duration of the three important notes
I_THREE_CLIMAX_DUR_MULTI = (1/PHI)**2;     % slow down factor
I_INTER_AT     = 47.875;
I_INTER_DUR    =  4.547;
I_A            = I_START;
I_B            = ((I_BRIDGE_POINT - I_START) * (I_PHI)) + I_START;
I_C            = I_BRIDGE_POINT - (LEE * 3);
I_D            = I_BRIDGE_POINT + LEE;
I_E            = I_BRIDGE_POINT + (I_THREE_CLIMAX_NOTES_DUR*I_THREE_CLIMAX_DUR_MULTI);
I_G            = I_INTER_AT - (LEE*3);
I_H            = I_INTER_AT + LEE;
I_F            = ((I_INTER_AT - I_E) * (I_PHI)) + I_E;
I_I            = I_INTER_AT + I_INTER_DUR;
I_L            = I_I + LEE;
I_END          = I_L;
I_Y_COORDS     = [I_A I_B I_C I_D I_E I_F I_G I_H I_I I_L];
%
% VII (output) section
%
VII_DURATION = 14;     % let's say it'll last 14 minutes
VII_DUR      = 14*60;
VII_CLIMAX   = VII_DUR * PHI;
VII_INTER_DUR = I_INTER_DUR * 2;
VII_A        = 0.0;
VII_B        = VII_CLIMAX * (1 - (PHI**1));
VII_C        = VII_CLIMAX - LEE;
VII_D        = VII_CLIMAX + LEE;
VII_E        = (VII_D-LEE) + I_THREE_CLIMAX_NOTES_DUR * I_THREE_CLIMAX_DUR_MULTI;
VII_L        = VII_DUR;
VII_I        = VII_L - (LEE * 4);
VII_G        = VII_I - VII_INTER_DUR - LEE;
VII_H        = VII_G + (LEE * 2);
VII_F        = (((VII_G + LEE) - VII_E) * (1 - (PHI**2))) + VII_E;
VII_X_COORDS = [VII_A VII_B VII_C VII_D VII_E VII_F VII_G VII_H VII_I VII_L];

