%
% calculates the stretching function for the third conditioning
%
VII_CLIMAX_DUR = I_THREE_CLIMAX_NOTES_DUR*I_THREE_CLIMAX_DUR_MULTI; % slow down
TABLE_SIZE = 2**15;
TIME_STEP = VII_DUR/TABLE_SIZE;     % sampling
T = [0:TIME_STEP:VII_DUR-TIME_STEP];

fun = interp1(VII_X_COORDS, I_Y_COORDS, T, "pchip");
