<CsoundSynthesizer>
;
; Third conditioning step: stretch the combined file
;
;<CsOptions>
;</CsOptions>
; ==============================================
<CsInstruments>
	
sr	=	44100
kr	=	441
ksmps	=	100
nchnls =	2
0dbfs  = 8388607

#include "constants.inc"

        instr 1	
ifleft  = 1
ifright = 2
idur    = p3
iodur   = p4
ifreq   = 1/idur
ifun    = 1
iflen   = ftlen(ifun)
itune   = 1
igain   = ampdb(-1)

kfreader phasor ifreq
ktimpt   table  kfreader, ifun, 1

aleft    pvoc   ktimpt, itune, ifleft
aright   pvoc   ktimpt, itune, ifright
aleft    linen  aleft, 0.2, idur, 0.01
aright   linen  aright, 0.2, idur, 0.01

ktime    times
         fprintks "pvoc_ktimpt.data", "%10.6f %20.18f %05d\n", ktime, ktimpt, int(kfreader*iflen)

         outs   aleft*igain, aright*igain
         endin


</CsInstruments>
; ==============================================
<CsScore>

f1 0 32768 -23 "stretch_function.inc"

#include "pvoc_score_line.inc"

</CsScore>
</CsoundSynthesizer>

