# Variazioni-II (*Brutte Melanconie Nostalgiche*)

Sources for the *Variazioni II (Brutte Melanconie Nostalgiche)* composition.

## Requirements

* python (as of 2018 I am using python3)
* the essentia library from https://github.com/MTG/essentia, with all the
  requirements to compile that

## Cooking Instructions

In order to create this piece, you have to invent a recepie for a pie (or
maybe a pizza, or rather a sort of home-made spaghetti).

You have to prepare a dough mixing together a number of ingredients that you may find
[here](https://gitlab.com/nicb/Variazioni-II/tree/master/sources/originals).

I am currently preparing the dough. With a number of ingredients - as a matter
of fact, they are different versions of the same ingredient. As if they were
many different kind of flour.

I will then lay it out and make long narrow frequency stripes out of it. Then
I will cook it and it will finally be ready.

It will be covered then with a sauce that will be created by the parts of five
traditional instruments: piano, vln, cello, flute, bs cl. The sauce will
stress and focus on some characteristics of the dough, while the dough will
change and modify itself according to some needs of the instrumental sauce.

No big deal up to here.

The issue is that, differently from cooking, the dough is far from homogenous:
when it is laid down, it resembles more to a geographic map than to pizza
dough. There will be different zones, so the stripes will have different
materials.

The stripes will not be cut in a geometric and homogenous way. They will be
not equidistant among them, they will follow their own geometries and will be
getting denser towards the center. Right after the center, there will be a
compact and homogenous zone that will not be striped at all and which will
allow a glimpse of a small musical gesture which may be easily recognized. It
will be the result of precise matchings of the dough, as it it had became a
mosaic.

This zone will then be covered, towards the end, by an over-abundant quantity
of "strong" sauce which will cover the underlaying design completely. The
small stripes that will survive will conclude the work.
